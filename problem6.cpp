#include <iostream>

// (\sum_{1}^{100} n)^2 - \sum_{1}^{100} n^2 = 2 (\sum_{1 \le i, j \le 100 \and i \neq j} ij)
int main() {
  int ans = 0;

  for (int i = 1; i <= 100; ++i) {
    for (int j = i + 1; j <= 100; ++j) {
      ans += i * j;
    }
  }
  ans *= 2;

  std::cout << ans << std::endl;

  return 0;
}
