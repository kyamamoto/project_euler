#include <cmath>
#include <iostream>
#include <vector>

// 素数判定
bool isPrime(int n) {
  // 2 以外の偶数は false
  if (n != 2 && n % 2 == 0) {
    return false;
  }

  // sqrt(n) 以下の奇数で割り切れるものがあれば false
  for (int i = 3; i <= static_cast<int>(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main() {
  const int upper_limit = 20;

  // 素数列を作成
  std::vector<int> primes;
  for (int i = 2; i <= upper_limit; ++i) {
    if (isPrime(i)) {
      primes.push_back(i);
    }
  }

  // 各素因数の指数を計算
  std::vector<int> exponents(primes.size());
  for (size_t i = 0; i < primes.size(); ++i) {
    if (primes.at(i) <= sqrt(upper_limit)) {
      exponents.at(i) = log(upper_limit) / log(primes.at(i));
    } else {
      exponents.at(i) = 1;
    }
  }

  int ans = 1;
  for (size_t i = 0; i < primes.size(); ++i) {
    ans *= pow(primes.at(i), exponents.at(i));
  }

  std::cout << ans << std::endl;

  return 0;
}
