#include <iostream>

int main() {
  const int upper_limit = 4000000;
  int f1 = 1, f2 = 2, sum = 0;

  while (true) {
    if (f2 % 2 == 0) {
      sum += f2;
    }

    // 項の更新
    int tmp = f2;
    f2 = f1 + f2;
    f1 = tmp;

    if (f2 >= upper_limit) {
      break;
    }
  }

  std::cout << sum << std::endl;

  return 0;
}
