#include <cmath>
#include <iostream>

// 素数判定
bool isPrime(int n) {
  // 2 以外の偶数は false
  if (n != 2 && n % 2 == 0) {
    return false;
  }

  // sqrt(n) 以下の奇数で割り切れるものがあれば false
  for (int i = 3; i <= static_cast<int>(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main() {
  const long long int given_num = 600851475143LL;
  int max = 0;

  // 最大の「素」因数を見つけるので、sqrt(given_num) まで探索
  for (int i = 2; i <= static_cast<int>(sqrt(given_num)); ++i) {
    if (isPrime(i) && given_num % i == 0) {
      max = i;
    }
  }

  std::cout << max << std::endl;

  return 0;
}
