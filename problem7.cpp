#include <cmath>
#include <iostream>

// 素数判定
bool isPrime(int n) {
  // 2 以外の偶数は false
  if (n != 2 && n % 2 == 0) {
    return false;
  }

  // n 未満の奇数で割り切れるものがあれば false
  for (int i = 3; i <= static_cast<int>(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main() {
  const int given_num = 10001;
  int i = 2, n = 0;

  while (true) {
    if (isPrime(i)) {
      ++n;
    }

    if (n == given_num) {
      std::cout << i << std::endl;
      break;
    }

    ++i;
  }

  return 0;
}
