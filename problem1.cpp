#include <iostream>

int main() {
  const int upper_limit = 1000;
  int sum = 0;

  for (int i = 1; i < upper_limit; ++i) {
    if (i % 3 == 0 || i % 5 == 0) {
      sum += i;
    }
  }

  std::cout << sum << std::endl;

  return 0;
}
