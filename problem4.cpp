#include <iostream>
#include <vector>

// 回文数判定
bool isPalindrome(std::vector<int> v) {
  for (size_t i = 0, n = v.size() / 2; i <= n; ++i) {
    if (v.at(i) != v.at(v.size() - 1 - i)) {
      return false;
    }
  }
  return true;
}

int main() {
  int max = 0;

  for (int i = 999; i >= 100; --i) {
    for (int j = i; j >= 100; --j) {
      const int n = i * j;

      // n の各桁の値を vector に格納
      int q = n / 10;
      int r = n % 10;
      std::vector<int> v;
      while (true) {
        v.push_back(r);

        if (q == 0) {
          break;
        }

        r = q % 10;
        q /= 10;
      }

      if (isPalindrome(v) && max < n) {
        max = n;
        break;
      }
    }
  }

  std::cout << max << std::endl;

  return 0;
}
